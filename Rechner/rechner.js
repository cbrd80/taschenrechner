function addieren(x, y) {
    return (x + y);
}
function subtrahieren(x, y) {
    return (x - y);
}
function multiplizieren(x, y) {
    return (x * y);
}
function dividieren(x, y) {
    return (x / y);
}
function Flaecheninhalt(x, y) {
    return (x * y + "m²");
}
function Umfang(x, y) {
    return 2 * addieren(x, y);
}
function Umfangkreis(x, y) {
    return 2 * 3.1415926 * x;
}
function Potenzieren(x, y) {
    if (y == 0) {
        return 1;
    }
    else {
        return Math.pow(x, y);
    }
}
function Eulersche(x, y) {
    return (Math.pow((1 + (1 / x)), x));
}
function kreis(x, y) {
    return (3.1415926 * Math.pow(x, 2) + "m²");
}
function Fakultät(x, i) {
    if (x == 0) {
        return 1;
    }
    else {
        for (i = 1; i <= x; i++)
            return x * i * (x - 1);
    }
}
function Modulo(x, y) {
    return ("Rest:" + x % y);
}
function Dezimal(x, dual, i) {
    while (x >= 1) {
        dual = x % 2;
        x = x / 2;
        i = i + dual;
    }
    return i;
}
$("#addieren").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    $("#Ergebnis").text(addieren(x, y));
});
$("#subtrahieren").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    $("#Ergebnis").text(subtrahieren(x, y));
});
$("#multiplizieren").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    $("#Ergebnis").text(multiplizieren(x, y));
});
$("#dividieren").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    $("#Ergebnis").text(dividieren(x, y));
});
$("#Flächeninhalt").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    $("#Ergebnis").text(Flaecheninhalt(x, y));
});
$("#Umfang").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    $("#Ergebnis").text(Umfang(x, y));
});
$("#Potenzieren").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    $("#Ergebnis").text(Potenzieren(x, y));
});
$("#kreis").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    var i;
    $("#Ergebnis").text(kreis(x, y));
});
$("#Fakultät").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    $("#Ergebnis").text(Fakultät(x, y));
});
$("#Eulersche").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    $("#Ergebnis").text(Eulersche(x, y));
});
$("#Modulo").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    $("#Ergebnis").text(Modulo(x, y));
});
$("#Dezimal").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    var dual = 0;
    var i = 0;
    $("#Ergebnis").text(Dezimal(x, y, i));
});
$("#Umfangkreis").on('click', function () {
    var x = Number($("#x").val());
    var y = Number($("#y").val());
    $("#Ergebnis").text(Umfangkreis(x, y));
});
