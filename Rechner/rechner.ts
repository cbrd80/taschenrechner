function addieren(x:number, y:number):number{
    return (x+y);
}
function subtrahieren(x:number, y:number):number{
    return (x-y);
}
function multiplizieren(x:number, y:number):number{
    return (x*y);
}
function dividieren(x:number, y:number):number{
    return (x/y);
}
function Flaecheninhalt(x:number, y:number):string {
    return (x * y+ "m²");
}
function Umfang(x:number, y:number):number{
    return 2*addieren(x,y);
}
function Umfangkreis(x:number, y:number):number{
    return 2*3.1415926*x;
}



function Potenzieren(x:number, y:number, ):number {

    if (y == 0) {
        return 1;

    } else {

            return x**y;
    }
}

function Eulersche(x:number, y:number):number{
    return ((1+(1/x))**x);
}

function kreis(x:number, y:number):string {
    return (3.1415926* Math.pow(x,2)+ "m²");

}
function Fakultät(x: number, i:number): number {
    if (x == 0) {
        return 1;
    } else {

        for(i=1; i<= x; i++)
        return x * i*(x - 1);
    }
}
function Modulo(x:number, y:number):string {
    return ("Rest:"+ x % y);

}
function Dezimal(x:number, dual:number, i:number):number {


    while (x >=1) {
        dual = x % 2;
        x = x / 2;
        i = i + dual;
    }
    return i;
}




$("#addieren").on('click',()=>{
    let x: number= Number($("#x").val());
    let y: number= Number($("#y").val());

    $("#Ergebnis").text(addieren(x,y));

});


$("#subtrahieren").on('click',()=>{
    let x: number= Number($("#x").val());
    let y: number= Number($("#y").val());

$("#Ergebnis").text(subtrahieren(x,y));

});

$("#multiplizieren").on('click',()=>{
    let x: number= Number($("#x").val());
    let y: number= Number($("#y").val());

    $("#Ergebnis").text(multiplizieren(x,y));

});

$("#dividieren").on('click',()=>{
    let x: number= Number($("#x").val());
    let y: number= Number($("#y").val());

    $("#Ergebnis").text(dividieren(x,y));

});

$("#Flächeninhalt").on('click',()=> {
    let x: number = Number($("#x").val());
    let y: number = Number($("#y").val());

    $("#Ergebnis").text(Flaecheninhalt(x, y));
});

$("#Umfang").on('click',()=> {
    let x: number = Number($("#x").val());
    let y: number = Number($("#y").val());

    $("#Ergebnis").text(Umfang(x, y));
});
$("#Potenzieren").on('click',()=> {
    let x: number = Number($("#x").val());
    let y: number = Number($("#y").val());


    $("#Ergebnis").text(Potenzieren(x, y,));
});
$("#kreis").on('click',()=> {
    let x: number = Number($("#x").val());
    let y: number = Number($("#y").val());
    let i: number;
    $("#Ergebnis").text(kreis(x, y));
});

$("#Fakultät").on('click', () => {
    let x: number = Number($("#x").val());
    let y: number = Number($("#y").val());

    $("#Ergebnis").text(Fakultät(x, y) );
});

$("#Eulersche").on('click', () => {
    let x: number = Number($("#x").val());
    let y: number = Number($("#y").val());

    $("#Ergebnis").text(Eulersche(x, y) );
});

$("#Modulo").on('click', () => {
    let x: number = Number($("#x").val());
    let y: number = Number($("#y").val());

    $("#Ergebnis").text(Modulo(x, y) );
});

$("#Dezimal").on('click', () => {
    let x: number = Number($("#x").val());
    let y: number = Number($("#y").val());
    let dual: number=0;
    let i: number=0;

    $("#Ergebnis").text(Dezimal(x,y,i) );
});

$("#Umfangkreis").on('click', () => {
    let x: number = Number($("#x").val());
    let y: number = Number($("#y").val());

    $("#Ergebnis").text(Umfangkreis(x, y));
});